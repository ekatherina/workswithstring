//
//  ViewController.swift
//  String
//
//  Created by Katherina🌹 on 3/12/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //  countLettersInName(name: "Kate")
        // patronymicСheck(patronymic: "Antonovich")
        // fullName(str: "KateTitova")
        // reverseWords(phrase: "Good Morning!")
        //formatedNumber(number:"12131415161718")
        // passwordSecureLevel(password:"1234567")
        sortMassive()
        translitTransformer(word: "кирпич")
        
    }
    
    //1 counting letters in name
    func countLettersInName(name: String) {
        let name = name
        print ("Number of letters in the name \(name):",name.count)
    }
    
    //2 check patronymic for suffix
    func patronymicСheck(patronymic: String) {
        if patronymic.lowercased().hasSuffix("na") {
            print ("Patronymic \(patronymic) has suffix na" )
        }
        else if patronymic.lowercased().hasSuffix("ich") {
            print("Patronymic \(patronymic) has suffix ich" )
        }else{
            print("Mistaken patronymic in \(patronymic)")
        }
    }
    
    //3 drop full name
    func fullName(str: String) {
        let fullName = str
        let index = fullName.index(of: "T") ?? fullName.endIndex
        let name = fullName [..<index]
        let surname = fullName [index...]
        var correct = ""
        for f in fullName {
            if String(f) == String(f).uppercased() {
                correct.append(" ")
            }
            correct.append(f)
        }
        print("Name= \(name)\nSurname= \(surname)\nFull name=\(correct)")
    }
    
    //4 reverse words
    func reverseWords (phrase: String) {
        let string = phrase
        var reverseString = ""
        for str in string {
            reverseString.insert(str, at: reverseString.startIndex)
        }
        print("Revers incoming phrase \(phrase) is \(reverseString)")
    }
    
    //5 formated number
    func formatedNumber (number: String){
        var formated = ""
        for (index, element) in number.reversed().enumerated() {
            formated.insert(element, at: formated.startIndex)
            if((index + 1) % 3 == 0 && index != number.count - 1) {
                formated.insert(",", at: formated.startIndex)
            }
        }
        print("Correct formated number \(number) = \(formated)")
    }
    
    //6 password secure level
    func passwordSecureLevel(password: String){
        var secureLevel = 0
        let specialChar = password.rangeOfCharacter(from: CharacterSet.alphanumerics.inverted) != nil
        let upperChar = password.rangeOfCharacter(from: CharacterSet.uppercaseLetters) != nil
        let lowChar = password.rangeOfCharacter(from: CharacterSet.lowercaseLetters) != nil
        let number = password.rangeOfCharacter(from: .decimalDigits) != nil
        if number {
            secureLevel += 1
        }
        if  lowChar{
            secureLevel += 1
        }
        if  upperChar{
            secureLevel += 1
        }
        if  specialChar {
            secureLevel += 1
        }
        if  password.count >= 8{
            secureLevel += 1
        }
        print ("Password secure level - \(secureLevel)")
    }
    
    //7 sort massive
    func sortMassive() {
        let arrays =  [4,14,4,67,123,15,1,3,2,6,4,7,9,11,19]
        let  result = insert(clearDublicat(array: arrays))
        print(result)
    }
    func clearDublicat(array: [Int]) -> [Int] {
        return Array(Set(array))
    }
    func insert(_ array: [Int]) -> [Int] {
        var result = array
        for x in 1..<result.count {
            var y = x
            while y > 0 && result[y] < result[y - 1] {
                result.swapAt(y - 1, y)
                y -= 1
            }
        }
        return result
    }
    
    //8 translit word
    func translitTransformer(word: String){
        let incomingText = word
        let arrayWord = Array.init(incomingText)
        var resultWord = [Character]()
        let russianAlphabet = Array.init("абвгдезийклмнопрстуфхцэ")
        let translit = Array.init("abvgdezijklmnoprstufhce")
        let exeptions = Array.init("жчшщюяь")
        for x in 0..<arrayWord.count {
            for y in 0..<russianAlphabet.count {
                if arrayWord[x] == russianAlphabet[y] {
                    resultWord.append(translit[y])
                }
            }
            for e in 0..<exeptions.count {
                if arrayWord[x] == exeptions[e]{
                    switch e {
                    case 0:
                        resultWord.append("z")
                        resultWord.append("h")
                    case 1:
                        resultWord.append("c")
                        resultWord.append("h")
                    case 2:
                        resultWord.append("s")
                        resultWord.append("h")
                    case 3:
                        resultWord.append("s")
                        resultWord.append("c")
                        resultWord.append("h")
                    case 4:
                        resultWord.append("j")
                        resultWord.append("u")
                    case 5:
                        resultWord.append("j")
                        resultWord.append("a")
                    default:
                        print("What's wrong")
                    }
                }
            }
        }
        print("Translit word \(incomingText) is" ,String(resultWord))
    }
    
    //9 find symbols
    func findSymbols() {
        let incomingArray = ["lada", "sedan", "baklazhan"]
        let seachPhrase = "da"
        var outgoingArray = [String]()
        let searchWordSet = Set<Character>(seachPhrase)
        let predicate = NSPredicate(format: "SELF contains %@", seachPhrase)
        let search = incomingArray.filter {
            predicate.evaluate(with:$0)
        }
        print(search.sorted())
        for i in 0..<incomingArray.count {
            let setFromString = Set<Character>(incomingArray[i])
            if setFromString.isSuperset(of: searchWordSet) {
                outgoingArray.append(incomingArray[i])
            }
        }
        print(outgoingArray.sorted())
    }
    
}


